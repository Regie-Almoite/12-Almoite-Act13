const http = require("http");
const port = 3000;

http.createServer((req, res) => {
    if (req.url === "/login") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("You are in the login page");
    }
}).listen(port, () => {
    console.log(`Server successfully running on port ${port}`);
});
