// 1. What directive is used by Node.js in loading the modules it needs
//  - Require directive is used to load a node module

// 2. What Node.js module contains a method for server creation
//  - http module

// 3. What method of the response object allows us to set status codes and content types?
//  - writeHead() method

// 4. Where will console.log() output its contents when run in Node.js?
//  - the output will be displayed in the terminal

// 5. What property of the request object contains the address's endpoint?
//  - in the url property
